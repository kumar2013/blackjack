"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.playUntilWon = exports.checkBlackJack = exports.drawInitialCards = void 0;
var calculatePoints = function (value) {
    var points = { J: 10, Q: 10, K: 10, A: 11 };
    return parseInt(value) || points[value];
};
var getCardInfo = function (card) {
    return card.suit.charAt(0) + card.value;
};
var drawCard = function (player, cards) {
    var card = cards.shift();
    if (card) {
        player.addPoints(calculatePoints(card.value));
        player.addCard(getCardInfo(card));
    }
};
var drawInitialCards = function (players, cards) {
    players.forEach(function (player) {
        for (var i = 1; i < 3; i++) {
            drawCard(player, cards);
        }
    });
};
exports.drawInitialCards = drawInitialCards;
var checkBlackJack = function (players) {
    var result = {
        won: false,
        name: ''
    };
    players.forEach(function (player) {
        if (player.getPoints() == 21) {
            result.won = true;
            result.name = player.getName();
        }
    });
    return result;
};
exports.checkBlackJack = checkBlackJack;
var playUntilWon = function (players, cards) {
    var winner = null;
    var guestTurn = true;
    var bobTurn = false;
    var guest = players[0];
    var bob = players[1];
    while (!winner) {
        if (guestTurn && guest.getPoints() < 21) {
            drawCard(guest, cards);
            if (guest.getPoints() > 21) {
                winner = bob.getName();
                break;
            }
            if (guest.getPoints() >= 17) {
                guestTurn = false;
                bobTurn = true;
            }
        }
        if (bobTurn && bob.getPoints() < 21) {
            drawCard(bob, cards);
            if (bob.getPoints() > 21) {
                winner = guest.getName();
                break;
            }
            if (bob.getPoints() > guest.getPoints()) {
                bobTurn = false;
                guestTurn = true;
            }
        }
    }
    return winner;
};
exports.playUntilWon = playUntilWon;
