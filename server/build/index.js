"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var express_1 = __importDefault(require("express"));
var morgan_1 = __importDefault(require("morgan"));
var cors_1 = __importDefault(require("cors"));
var helmet_1 = __importDefault(require("helmet"));
var gameController = __importStar(require("./controllers/game-controller"));
var error_handler_1 = require("./middleware/error-handler");
var app = (0, express_1.default)();
var PORT = process.env.PORT || 6004;
// Set security headers
app.use((0, helmet_1.default)());
// Enable CORS
app.use((0, cors_1.default)());
// Enable req Logger
app.use((0, morgan_1.default)('tiny'));
// Routes
app.get('/', function (req, res) {
    res.send('Hello guys, please goto /game-results to get blackjack results');
});
app.get('/game-results', gameController.playGame);
// Catch 404 and forward to error handler
app.use(error_handler_1.handle404Errors);
// Custom server error handler
app.use(error_handler_1.handleServerErrors);
// Start the server
var server = app.listen(PORT, function () {
    console.log('*************************************************');
    console.log("  Server is listening at http://localhost:" + PORT + "  ");
    console.log('*************************************************');
});
// Gracefully shutting down the server
var exitEvents = [
    { type: 'SIGINT', code: 0 },
    { type: 'SIGTERM', code: 0 },
    { type: 'uncaughtException', code: 1 },
    { type: 'unhandledRejection', code: 1 }
];
exitEvents.forEach(function (event) {
    process.on(event.type, gracefullyExitServer.bind(null, { exit: true, eventType: event.type }, event.code));
});
function gracefullyExitServer(options, exitCode) {
    if (exitCode === void 0) { exitCode = 0; }
    console.log(options.eventType + " signal received with ExitCode " + exitCode);
    if (options.exit) {
        console.log('Closing the HTTP server...');
        server.close(function () {
            console.log('HTTP Server is closed.');
            process.exit(exitCode);
        });
    }
}
