"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Player = void 0;
var Player = /** @class */ (function () {
    function Player(name) {
        this.name = name;
        this.points = 0;
        this.cards = [];
    }
    Player.prototype.addPoints = function (points) {
        this.points = this.points + points;
    };
    Player.prototype.addCard = function (card) {
        this.cards.push(card);
    };
    Player.prototype.getName = function () {
        return this.name;
    };
    Player.prototype.getPoints = function () {
        return this.points;
    };
    Player.prototype.getCards = function () {
        return this.cards;
    };
    Player.prototype.getAllDetails = function () {
        return {
            name: this.name,
            points: this.points,
            cards: this.cards
        };
    };
    return Player;
}());
exports.Player = Player;
