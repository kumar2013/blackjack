"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.handleServerErrors = exports.handle404Errors = void 0;
var http_errors_1 = __importDefault(require("http-errors"));
var handle404Errors = function (req, res, next) {
    return next((0, http_errors_1.default)(404, "Requested url couldn't be found"));
};
exports.handle404Errors = handle404Errors;
var handleServerErrors = function (err, req, res, next) {
    if (err) {
        return res.status(err.status || 500).json({
            errors: [{
                    status: err.status || 500,
                    title: err.status === 404 ? 'Resource Not Found' : 'Internal Server Error',
                    detail: err.status ? err.message : 'Something went wrong in the Server'
                }]
        });
    }
    return next();
};
exports.handleServerErrors = handleServerErrors;
