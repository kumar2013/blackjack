import { describe, test, beforeEach, afterEach } from '@jest/globals';

import { drawInitialCards, checkBlackJack, playUntilWon } from './game.helper';
import { cardsMock } from '../../../mocks/test.mocks';
import { Player } from '../../models/player';
import { Card } from './game.helper.types';

describe('Game Helper', () => {
    let players: Player[] = [];
    let cards: Card[] = [];

    const getCardMocks = () => {
        return [...cardsMock];
    }

    beforeEach(() => {
        cards = getCardMocks();
        players.push(new Player('TestPlayer1'));
        players.push(new Player('TestPlayer2'))
    });

    afterEach(() => {
        cards = [];
        players = [];
    })

    test('should drawInitialCards', () => {
        expect(players.length).toBe(2);
        expect(players).toEqual([
            {
                name: 'TestPlayer1',
                points: 0,
                cards: []
            },
            {
                name: 'TestPlayer2',
                points: 0,
                cards: []
            }
        ]);

        drawInitialCards(players, cards);

        expect(players).toEqual([
            {
                name: 'TestPlayer1',
                points: 12,
                cards: ['D4', 'H8']
            },
            {
                name: 'TestPlayer2',
                points: 11,
                cards: ['D3', 'D8']
            }
        ]);
    });

    test('should checkBlackjack and returns no one', () => {
        drawInitialCards(players, cards);
        const blackJack = checkBlackJack(players);

        expect(blackJack.won).toBe(false);
        expect(players[0].getPoints()).not.toBe(21);
        expect(players[1].getPoints()).not.toBe(21);
    });

    test('should playUntilWon and the winner is TestPlayer2', () => {
        drawInitialCards(players, cards);
        const winner = playUntilWon(players, cards);

        expect(winner).toBe('TestPlayer2');
        expect(players[0].getPoints()).toBe(23);
        expect(players[1].getPoints()).toBe(11);
    });
});