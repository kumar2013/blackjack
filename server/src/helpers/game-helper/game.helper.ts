import { Player } from '../../models/player';
import { Points, Card } from './game.helper.types';


const calculatePoints = (value: string): number => {
    const points: Points = { J: 10, Q: 10, K: 10, A: 11 };

    return parseInt(value) || points[value];
}

const getCardInfo = (card: Card): string => {
    return card.suit.charAt(0) + card.value;
}

const drawCard = (player: Player, cards: Card[]): void => {
    const card = cards.shift();

    if (card) {
        player.addPoints(calculatePoints(card.value));
        player.addCard(getCardInfo(card));
    }
}

export const drawInitialCards = (players: Player[], cards: Card[]): void => {
    players.forEach(player => {
        for (let i = 1; i < 3; i++) {
            drawCard(player, cards);
        }
    });
}

export const checkBlackJack = (players: Player[]) => {
    const result = {
        won: false,
        name: ''
    };

    players.forEach(player => {
        if (player.getPoints() == 21) {
            result.won = true;
            result.name = player.getName();
        }
    });

    return result;
}

export const playUntilWon = (players: Player[], cards: Card[]): string => {
    let winner = null;
    let guestTurn = true;
    let bobTurn = false;
    const guest = players[0];
    const bob = players[1];

    while (!winner) {
        if (guestTurn && guest.getPoints() < 21) {
            drawCard(guest, cards);

            if (guest.getPoints() > 21) {
                winner = bob.getName();

                break;
            }

            if (guest.getPoints() >= 17) {
                guestTurn = false;
                bobTurn = true;
            }
        }

        if (bobTurn && bob.getPoints() < 21) {
            drawCard(bob, cards);

            if (bob.getPoints() > 21) {
                winner = guest.getName();

                break;
            }

            if (bob.getPoints() > guest.getPoints()) {
                bobTurn = false;
                guestTurn = true;
            }
        }
    }

    return winner;
}
