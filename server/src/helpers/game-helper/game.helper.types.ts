export type Card = {
    suit: string,
    value: string
}

export interface Points {
    [key: string]: number
}