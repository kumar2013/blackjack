import express from 'express';
import morgan from 'morgan';
import cors from 'cors';
import helmet from 'helmet';
import { Request, Response } from 'express';

import * as gameController from './controllers/game-controller';
import { handle404Errors, handleServerErrors } from './middleware/error-handler';

const app = express();
const PORT = process.env.PORT || 6004;

// Set security headers
app.use(helmet());

// Enable CORS
app.use(cors());

// Enable req Logger
app.use(morgan('tiny'));

// Routes
app.get('/', (req: Request, res: Response) => {
    res.send('Hello guys, please goto /game-results to get blackjack results');
});

app.get('/game-results', gameController.playGame);

// Catch 404 and forward to error handler
app.use(handle404Errors);

// Custom server error handler
app.use(handleServerErrors);

// Start the server
const server = app.listen(PORT, () => {
    console.log('*************************************************');
    console.log(`  Server is listening at http://localhost:${PORT}  `);
    console.log('*************************************************');
});

// Gracefully shutting down the server
const exitEvents = [
    { type: 'SIGINT', code: 0 },
    { type: 'SIGTERM', code: 0 },
    { type: 'uncaughtException', code: 1 },
    { type: 'unhandledRejection', code: 1 }
];

exitEvents.forEach((event) => {
    process.on(event.type, gracefullyExitServer.bind(null, { exit: true, eventType: event.type }, event.code));
});

type OptionsType = {
    exit: boolean;
    eventType: string;
};

function gracefullyExitServer(options: OptionsType, exitCode = 0) {
    console.log(`${options.eventType} signal received with ExitCode ${exitCode}`);

    if (options.exit) {
        console.log('Closing the HTTP server...');

        server.close(() => {
            console.log('HTTP Server is closed.');
            process.exit(exitCode);
        });

    }
}
