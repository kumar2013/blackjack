import axios from 'axios';

import { Card } from '../../helpers/game-helper/game.helper.types';

export const getDeckOfCards = async (url: string) => {
    try {
        const response = await axios.get<Card[]>(url);

        return response.data;
    } catch (error) {
        console.error(error);
    }
}