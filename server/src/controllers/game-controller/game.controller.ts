import createError from 'http-errors';
import { NextFunction, Request, Response } from 'express';

import { Player } from '../../models/player';
import {
    drawInitialCards,
    checkBlackJack,
    playUntilWon
} from '../../helpers/game-helper';
import { getDeckOfCards } from '../../services/cards';
import { GameResponse } from './game.controller.types';


const sendResponse = (res: Response, { winner, players }: GameResponse) => {
    console.log('### Winner => ', JSON.stringify({ winner, players }));

    res.send({
        winner,
        players
    });
}

export const playGame = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
    try {
        const url = 'https://pickatale-backend-case.herokuapp.com/shuffle'
        const cards = await getDeckOfCards(url);

        if (!cards) {
            console.log('could not access and retrive cards from external service');
            throw createError(500, 'Internal Server Error');
        }

        const guest = new Player('You');
        const bob = new Player('Bob');

        const players = [guest, bob];

        drawInitialCards(players, cards);

        const blackJack = checkBlackJack(players);

        if (blackJack.won) {
            return sendResponse(res, { winner: blackJack.name, players });
        }

        const winner = playUntilWon(players, cards);

        sendResponse(res, { winner, players });
    } catch (error) {
        next(error);
    }
}
