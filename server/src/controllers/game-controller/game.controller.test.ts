import createError from 'http-errors';
import { describe, test, beforeEach } from '@jest/globals';
import { getMockReq, getMockRes } from '@jest-mock/express';

import { playGame } from './game.controller';
import { getDeckOfCards } from '../../services/cards';
import { cardsMock, blackjackCardsMock } from '../../../mocks/test.mocks';


jest.mock('../../services/cards');

describe('Game controller', () => {
    const req = getMockReq();
    const { res, next, mockClear } = getMockRes();

    beforeEach(() => {
        mockClear();
    });

    test('should play game and return results', async () => {
        (getDeckOfCards as jest.Mock).mockReturnValue(cardsMock);

        await playGame(req, res, next);

        expect(res.send).toHaveBeenCalledTimes(1);
        expect(res.send).toHaveBeenCalledWith({
            winner: "Bob",
            players: [
                {
                    name: "You",
                    points: 23,
                    cards: ["D4", "H8", "SA"]
                },
                {
                    name: "Bob",
                    points: 11,
                    cards: ["D3", "D8"]
                }
            ]
        });
    });

    test('should return blackjack', async () => {
        (getDeckOfCards as jest.Mock).mockReturnValue(blackjackCardsMock);

        await playGame(req, res, next);

        expect(res.send).toHaveBeenCalledTimes(1);
        expect(res.send).toHaveBeenCalledWith({
            winner: "You",
            players: [
                {
                    name: "You",
                    points: 21,
                    cards: ["DA", "HK",]
                },
                {
                    name: "Bob",
                    points: 11,
                    cards: ["D3", "D8"]
                }
            ]
        });
    });

    test('should throw Internal server error when problem retrieving cards', async () => {
        (getDeckOfCards as jest.Mock).mockReturnValue(null);

        await playGame(req, res, next);

        expect(res.send).toHaveBeenCalledTimes(0);
        expect(next).toHaveBeenCalledTimes(1);
        expect(next).toHaveBeenCalledWith(createError(500, 'Internal Server Error'));
    });
});