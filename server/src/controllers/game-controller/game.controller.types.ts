import { Player } from "../../models/player"

export type GameResponse = {
    winner: string,
    players: Player[]
}