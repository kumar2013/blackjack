import { describe, test, beforeEach } from '@jest/globals';

import { Player } from './player';

describe('Player', () => {
    const testPlayerName = 'TestPlayer';
    let testPlayer: Player;

    beforeEach(() => {
        testPlayer = new Player(testPlayerName);
    });

    test('should create a player instance', () => {
        expect(testPlayer.getName()).toBe(testPlayerName);
        expect(testPlayer.getPoints()).toBe(0);
        expect(testPlayer.getCards().length).toBe(0);
    });

    test('should add points to the instance', () => {
        expect(testPlayer.getPoints()).toBe(0);

        testPlayer.addPoints(5);

        expect(testPlayer.getPoints()).toBe(5);
    });

    test('should add card to the instance', () => {
        expect(testPlayer.getCards().length).toBe(0);

        testPlayer.addCard('H5');

        expect(testPlayer.getCards().length).toBe(1);
    });

    test('should return the points for instance', () => {
        expect(testPlayer.getPoints()).toBe(0);
    });

    test('should return the name of instance', () => {
        expect(testPlayer.getName()).toBe(testPlayerName);
    });

    test('should return the cards from the instance', () => {
        testPlayer.addCard('H5');

        expect(testPlayer.getCards()).toEqual(['H5']);
    });

    test('should return all details of the instance', () => {
        expect(testPlayer.getAllDetails()).toEqual({
            name: testPlayerName,
            points: 0,
            cards: []
        });
    });
});