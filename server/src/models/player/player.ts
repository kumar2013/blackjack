export class Player {
    private name: string;
    private points: number;
    private cards: string[];

    constructor(name: string) {
        this.name = name;
        this.points = 0;
        this.cards = [];
    }

    addPoints(points: number) {
        this.points = this.points + points;
    }

    addCard(card: string) {
        this.cards.push(card);
    }

    getName() {
        return this.name;
    }

    getPoints() {
        return this.points;
    }

    getCards() {
        return this.cards;
    }

    getAllDetails() {
        return {
            name: this.name,
            points: this.points,
            cards: this.cards
        };
    }
}