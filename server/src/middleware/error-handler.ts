import createError, { HttpError } from 'http-errors';
import { NextFunction, Request, Response } from 'express';

export const handle404Errors = (req: Request, res: Response, next: NextFunction) => {
    return next(createError(404, `Requested url couldn't be found`));
}


export const handleServerErrors = (err: HttpError, req: Request, res: Response, next: NextFunction) => {
    if (err) {
        return res.status(err.status || 500).json({
            errors: [{
                status: err.status || 500,
                title: err.status === 404 ? 'Resource Not Found' : 'Internal Server Error',
                detail: err.status ? err.message : 'Something went wrong in the Server'
            }]
        });
    }

    return next();
}
