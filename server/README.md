# BlackJack

## Steps to run the project

1). Download the repository into your local machine<br />
2). cd into the project folder and run "npm install". This will install all the dependencies.<br />
3). Once the dependencies are installed then run "yarn start" or "npm start". This will build the files and then start the server at http://localhost:6004<br />
4). Open the url in the browser or postman and goto http://localhost:6004/game-results to see the game results<br />
5). Run "yarn test" or "npm test" to launch the test runner <br />
6). Run "yarn lint" or "npm run lint" for linting<br />